### Docker/コンテナ？

---

### なんのために？

- 軽量で可搬性に優れた、独立したアプリケーション実行環境の実現。
- ライブラリ、フレームワークの相互干渉防止。
- Build, ship and run anywhere
- 輸送におけるコンテナの例え。
- VMはOSまでまるごと分離。コンテナは上のレイヤで分離。
- 自動化も組み合わせることで、開発〜リリースに至る一連の作業の効率化。

---

### 環境セットアップ

- www.docker.com
- Docker Desktop(Communiti edition)
    - Docker for Windows/Mac
    - Docker Toolbox は非推奨

---

### 簡単なテスト
- rmオプション＝コンテナ停止後に削除

```
docker container run --rm ubuntu echo hello
docker container run --rm -it ubuntu /bin/bash
```

---

### 簡単なステータス確認

```
docker container run ubuntu echo hello
docker container run -it ubuntu /bin/bash
```

別画面で・・・

```
docker container ls
docker container ls -a
docker container rm <target>
```

---

### Apacheを起動してみる

```
docker container run -dit --name my-apache-app -p 8080:80 -v "$PWD":/usr/local/apache2/htdocs/ httpd:2.4
```

---

### イメージの作成
- デモ。

---

https://gitpitch.com/tamahiko/docker-how-to/master?grs=gitlab
